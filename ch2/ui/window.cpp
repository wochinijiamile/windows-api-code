﻿/* ************************************
*¡¶¾«Í¨Windows API¡· 
* Ê¾Àý´úÂë
* window.cpp
* 2.2  Windows APIµÄ¹¦ÄÜ·ÖÀà
**************************************/

/* Ô¤´¦Àí¡¡*/
/* Í·ÎÄ¼þ¡¡*/
#include <windows.h>

/* È«¾Ö±äÁ¿¡¡*/
HINSTANCE hinst; 

/* º¯ÊýÉùÃ÷¡¡*/ 
int WINAPI WinMain(HINSTANCE, HINSTANCE, LPSTR, int); 
LRESULT CALLBACK MainWndProc(HWND, UINT, WPARAM, LPARAM); 

/* ************************************
* 本程序的功能就是显示一个窗口
**************************************/
int WINAPI WinMain(HINSTANCE hinstance, 
                   HINSTANCE hPrevInstance, 
                   LPSTR lpCmdLine, 
                   int nCmdShow) 
{     
    WNDCLASSEX wcx;         //窗口类，其实就是一个结构体
    HWND hwnd;              //窗口句柄
    MSG msg;                //消息
    BOOL fGotMessage;       //是否成功获取消息
    hinst = hinstance;      //应用程序实例句柄，是一个全局变量

    // 填充结构体的值
    wcx.cbSize = sizeof(wcx);          //获取结构体大小
    wcx.style = CS_HREDRAW | 
        CS_VREDRAW;                    //设置窗口样式，这两个值或在一起意思就是在竖直和水平方向上有移动或者尺寸的调整的话就对整个窗口进行重绘
    wcx.lpfnWndProc = MainWndProc;     // 窗口消息处理的回调函数
    wcx.cbClsExtra = 0;                //不使用类内存
    wcx.cbWndExtra = 0;                // 不使用窗口内存 （不太清楚这两个属性的作用）
    wcx.hInstance = hinstance;         //所属应用程序的实例句柄
    wcx.hIcon = LoadIcon(NULL, 
        IDI_APPLICATION);              //图标，使用的实windows自带的图标
    wcx.hCursor = LoadCursor(NULL, 
        IDC_ARROW);                    //使用默认的光标（鼠标箭头）
    wcx.hbrBackground = (HBRUSH)GetStockObject( 
        WHITE_BRUSH);                  // 设置背景，白色，返回的是一个刷子数据类型，后面应该还会使用该对象的某个方法来进行涂色
    wcx.lpszMenuName =  NULL;          // 菜单置空（不使用）
    wcx.lpszClassName = "MainWClass";  // 设置窗口类的名称，这个名称是提供给RegisterClassEx进行使用的，他会使用这个名称作为新创建出来的类的名称
    wcx.hIconSm = (HICON)LoadImage(hinstance, // 小图标
        MAKEINTRESOURCE(5),
        IMAGE_ICON, 
        GetSystemMetrics(SM_CXSMICON), 
        GetSystemMetrics(SM_CYSMICON), 
        LR_DEFAULTCOLOR); 

    // 注册窗口类，错误则直接返回
    if(!RegisterClassEx(&wcx))
    {
        return 1;
    }

    // 创建窗口
    hwnd = CreateWindow( 
        "MainWClass",        //刚才使用RegisterClassEx创建的类的类名，也就是wcx结构体的lpszClassName成员的值
        "CH 2-3",            // 窗口标题
        WS_OVERLAPPEDWINDOW, //窗口样式
        CW_USEDEFAULT,       // 水平位置：默认
        CW_USEDEFAULT,       // 垂直位置：默认
        CW_USEDEFAULT,       // 宽度：默认
        CW_USEDEFAULT,       // 高度：默认
        (HWND) NULL,         // 父窗口：无
        (HMENU) NULL,        // 菜单：置空（表示使用窗口类的菜单，窗口类就是用RegisterClassEx注册的那个类，菜单也是空的，所以最终创建出来的窗口的菜单栏还是空的）
        hinstance,           // 应用程序实例句柄，这个在wcx结构体中已经指明过一次了，不知道为啥这里还要再传入给CreateWindow函数一次
        (LPVOID) NULL);      //窗口创建时数据，无
	//如果创建不成功则直接返回（终止程序）
    if (!hwnd) 
    {
        return 1; 
    }

    //显示窗口
    ShowWindow(hwnd, nCmdShow); 
    UpdateWindow(hwnd); 
		
    //消息循环
    while (
        (fGotMessage = GetMessage(&msg, (HWND) NULL, 0, 0)) != 0 
        && fGotMessage != -1) 
    { 
        TranslateMessage(&msg); 
        DispatchMessage(&msg); 
    } 
    return msg.wParam; 

} 

/* ************************************
* MainWndProc
*窗口消息处理函数
对所有消息使用默认处理函数
**************************************/
LRESULT CALLBACK MainWndProc(HWND hwnd,
                             UINT uMsg,
                             WPARAM wParam,
                             LPARAM lParam
                             )
{
    switch (uMsg) 
    { 
		//这里貌似就写了一个窗口销毁消息的处理，其他的全使用默认的窗口处理函数DefWindowProc
        case WM_DESTROY: 
            ExitThread(0);
            return 0; 
        default: 
            return DefWindowProc(hwnd, uMsg, wParam, lParam); 
    } 
}