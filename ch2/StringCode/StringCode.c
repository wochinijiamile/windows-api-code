﻿/* ************************************
*¡¶¾«Í¨Windows API¡· 
* Ê¾Àý´úÂë
* StringCode.c
* 2.4	UnicodeºÍ¶à×Ö½Ú
**************************************/

/* Ô¤´¦Àí¡¡*/
/* Í·ÎÄ¼þ¡¡*/
#include <windows.h>
/* ************************************
* Unicode与多字节编码演示
**************************************/
int WINAPI WinMain(
			HINSTANCE hInstance,
			HINSTANCE hPrevInstance,
			LPSTR lpCmdLine,
			int nCmdShow
			)
{
    //LPWSTR是16位的unicode字符串的指针类型
	LPWSTR szUnicode = L"爸爸吧This is a Unicode String;";
    //LPSTR就是普通的char
    LPSTR szMutliByte = "滴滴滴";
    //TEXT可以根据用户是否声明了UNICODE宏来决定自己是代表Unicode字符串还是普通字符串
    LPTSTR szString = TEXT("这只是一个测试");

    //W代表Wide，即以宽字节（unicode）字符串作为参数
    MessageBoxW(NULL,szUnicode,L"<字符编码1>",MB_OK);
    //A代表Ascii，以ascii编码的字符串作为参数
    MessageBoxA(NULL,szMutliByte,"<字符编码2>",MB_OK);
    //根据是否声明Unicode宏来决定是采用W版本还是A版本
    MessageBox(NULL,szString,TEXT("<字符编码3>"),MB_OK);

	return 0;
}