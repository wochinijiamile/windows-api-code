﻿/* ************************************
 *¡¶¾«Í¨Windows API¡· 
 * Ê¾Àý´úÂë
 * GetVolumeInfo.c
 * 4.2.1	±éÀúÇý¶¯Æ÷²¢»ñÈ¡Çý¶¯Æ÷ÊôÐÔ
 **************************************/

/* Í·ÎÄ¼þ¡¡*/
#include <windows.h>
#include <stdlib.h>
#include <stdio.h>
/* Ô¤¶¨Òå¡¡*/
#define BUFSIZE 1024
/* º¯ÊýÉêÃ÷¡¡*/
BOOL GetDirverInfo(LPSTR szDrive);

/* ************************************
 * ¹¦ÄÜ	Ó¦ÓÃ³ÌÐòÖ÷º¯Êý£¬±éÀúÇý¶¯Æ÷²¢µ÷ÓÃ
 *			GetDirverInfo »ñÈ¡Çý¶¯Æ÷ÊôÐÔ
 **************************************/
void main(void)
{
	CHAR szLogicalDriveStrings[BUFSIZE];
	PCHAR szDrive;

	// 该函数用于初始化数组，就是将数组中的空间全部填充为0
	ZeroMemory(szLogicalDriveStrings,BUFSIZE);
	//对于该函数，官方文档上有明确说明对一个参数为数组缓冲区的长度，但是不包括终止符\0
	//因此在计算长度的时候需要减1，然后将获取到的盘符存储到该字符串数组中
	//返回值样例：C:\\\0D:\\\0H:\\\0，可以看到这些字符串之间使用\0进行了分割
	GetLogicalDriveStrings(BUFSIZE - 1,szLogicalDriveStrings);
	//本程序无法使用unicode字符集进行编译，因为有太多的函数会在此字符集下出现参数类型不符的问题
	szDrive = szLogicalDriveStrings;
	
	// Ñ­»·´¦ÀíÃ¿¸ö¾í
	do
	{
		if(!GetDirverInfo(szDrive))
		{
			printf("\nGet Volume Information Error: %d", GetLastError());
		}
		szDrive += (lstrlen(szDrive)+1);
	}
	while(*szDrive!='\x00');
}

/* ************************************
 * BOOL GetDirverInfo(LPSTR szDrive)
 * ¹¦ÄÜ	»ñÈ¡Çý¶¯Æ÷µÄÊôÐÔ
 * ²ÎÊý	LPSTR szDrive
 * 	Ö¸Ã÷Òª»ñÈ¡ÊôÐÔµÄÇý¶¯Æ÷µÄ¸ùÂ·¾¶ Èç C:\
 * ·µ»ØÖµ BOOL ÊÇ·ñ³É¹¦
 **************************************/
BOOL GetDirverInfo(LPSTR szDrive)
{
	UINT uDriveType;
	DWORD dwVolumeSerialNumber;
	DWORD dwMaximumComponentLength;
	DWORD dwFileSystemFlags;
	TCHAR szFileSystemNameBuffer[BUFSIZE];
	printf("\n%s\n",szDrive);
	uDriveType = GetDriveType(szDrive);
	// ÅÐ¶ÏÀàÐÍ
	switch(uDriveType)
	{
	case DRIVE_UNKNOWN:
		printf("The drive type cannot be determined. ");
		break;
	case DRIVE_NO_ROOT_DIR:
		printf("The root path is invalid, for example, no volume is mounted at the path. ");
		break;
	case DRIVE_REMOVABLE:
		printf("The drive is a type that has removable media, for example, a floppy drive or removable hard disk. ");
		break;
	case DRIVE_FIXED:
		printf("The drive is a type that cannot be removed, for example, a fixed hard drive. ");
		break;
	case DRIVE_REMOTE:
		printf("The drive is a remote (network) drive. ");
		break;
	case DRIVE_CDROM:
		printf("The drive is a CD-ROM drive. ");
		break;
	case DRIVE_RAMDISK:
		printf("The drive is a RAM disk. ");
		break;
	default:
		break;
	}
	if (!GetVolumeInformationA(
		szDrive, NULL, 0,
		&dwVolumeSerialNumber,
		&dwMaximumComponentLength,
		&dwFileSystemFlags,
		szFileSystemNameBuffer,
		BUFSIZE
		))
	{
		return FALSE;
	}
	printf ("\nVolume Serial Number is %u",dwVolumeSerialNumber);
	printf ("\nMaximum Component Length is %u",dwMaximumComponentLength);
	printf ("\nSystem Type is %s\n",szFileSystemNameBuffer);

	if(dwFileSystemFlags & FILE_SUPPORTS_REPARSE_POINTS)
	{
		printf ("The file system does not support volume mount points.\n");
	}
	if(dwFileSystemFlags & FILE_VOLUME_QUOTAS)
	{
		printf ("The file system supports disk quotas.\n");
	}
	if(dwFileSystemFlags & FILE_CASE_SENSITIVE_SEARCH)
	{
		printf ("The file system supports case-sensitive file names.\n");
	}
	//you can use these value to get more informaion
	//
	//FILE_CASE_PRESERVED_NAMES
	//FILE_CASE_SENSITIVE_SEARCH
	//FILE_FILE_COMPRESSION
	//FILE_NAMED_STREAMS
	//FILE_PERSISTENT_ACLS
	//FILE_READ_ONLY_VOLUME
	//FILE_SUPPORTS_ENCRYPTION
	//FILE_SUPPORTS_OBJECT_IDS
	//FILE_SUPPORTS_REPARSE_POINTS
	//FILE_SUPPORTS_SPARSE_FILES
	//FILE_UNICODE_ON_DISK
	//FILE_VOLUME_IS_COMPRESSED
	//FILE_VOLUME_QUOTAS
	printf("...\n");
	return TRUE;
}