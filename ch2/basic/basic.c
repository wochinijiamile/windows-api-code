﻿/* ************************************
*¡¶¾«Í¨Windows API¡· 
* Ê¾Àý´úÂë
* basic.c
* 2.2  Windows APIµÄ¹¦ÄÜ·ÖÀà
**************************************/

/* Í·ÎÄ¼þ¡¡*/
#include <windows.h>

/* ************************************
* ¹¦ÄÜ	»ñÈ¡ÏµÍ³Ä¿Â¼ÐÅÏ¢£¬²¢´æ´¢µ½ÎÄ¼þÖÐ
**************************************/
int main(int argc, TCHAR argv[])
{
    //ÎÄ¼þ¾ä±ú
    HANDLE hFile;

    DWORD dwWritten;
	//MAX_PATH值为260
	//没有声明UNICODE宏的TCHAR就是个CHAR
    TCHAR szSystemDir[MAX_PATH];
	//使用GetSystemDirectory函数获取系统根目录，结果保存在szSystemDir数组中
    GetSystemDirectory(szSystemDir,MAX_PATH);

    //创建文件systemroot.txt，待会儿将获取到的系统目录写入到该文件中：
    hFile = CreateFile("systemroot.txt",
        GENERIC_WRITE,
        0,NULL,CREATE_ALWAYS,
        FILE_ATTRIBUTE_NORMAL,
        NULL);
    //检测一下文件是否创建成功
    if(hFile != INVALID_HANDLE_VALUE)
    {
        //如果成功则将目录内容写入到文件中
        if(!WriteFile(hFile,szSystemDir,lstrlen(szSystemDir),&dwWritten,NULL))
        {
			//写入不成功返回错误信息
            return GetLastError();
        }
    }
    //关闭文件句柄
    CloseHandle(hFile);
    return 0;
}